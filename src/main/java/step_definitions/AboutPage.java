package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;

public class AboutPage {

    StringName stringName = new StringName();
    private final static By NAME = By.xpath("//input[@placeholder='Имя']");
    private final static By LAST_NAME = By.xpath("//input[@id='lastName']");
    private final static By EMAIL = By.xpath("//input[@id='userEmail']");
    private final static By GENDER = By.xpath("//input[@value='Хочу кошку']");
    private final static By NUMBER = By.xpath("//input[@id='userNumber']");
    private final static By DATA_TAKE = By.xpath("//input[@value='18 Aug 2022']");
    private final static By SCROLL = By.xpath("//label[@id='userNumber-label']");
    private final static By SIZE_DOG = By.xpath("//div//input[@id='hobbies-checkbox-2']");
    private final static By LANG_DOG = By.xpath("//*[text()='Java']");
    private final static By UPLOAD = By.xpath("//*[@id='загрузите картинку']");
    private final static By ADDRES = By.xpath("//*[@placeholder='Прописка']");
    private final static By SEND_BUTTON = By.xpath("//button[@type='submit']");

    @И("^заполнить форму бронирование питомца$")
    public void TheOrderForm() {
        $(SCROLL).scrollTo();
            $(NAME).sendKeys(stringName.firstName);
                $(LAST_NAME).sendKeys(stringName.lastName);
                    $(EMAIL).sendKeys(stringName.email);
                        $(GENDER).click();
                            $(NUMBER).sendKeys(stringName.phone);
                                $(SIZE_DOG).click();
                                    $(LANG_DOG).click();
                                        $(UPLOAD).uploadFile(new File("src/test/java/resources/passport.jpg"));
                                            $(ADDRES).click();
                                                $(SEND_BUTTON).click();
    }
}
