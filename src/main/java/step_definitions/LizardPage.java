package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class LizardPage {
    StringName stringName = new StringName();
    public final By NAME_ARTICLE = By.xpath("//h1");
    @И("^проверить название статьи$")
    public void selectProduct(String articleName) {
      $(NAME_ARTICLE).shouldNotHave(text("        Можно ли ящерице попасть в тестирование?    "));
    }
}
