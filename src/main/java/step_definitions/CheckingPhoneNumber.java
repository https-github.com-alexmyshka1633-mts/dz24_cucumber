package step_definitions;

import com.codeborne.selenide.Condition;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CheckingPhoneNumber {
    StringName stringName = new StringName();
    private final static By CHECK_NUMBER = By.xpath("//label[@id='userNumber-label']");
    @И("проверка номера телефона")
    public void makeSureTheOrderHasBeenSent() {
        $(CHECK_NUMBER).shouldHave(Condition.text(stringName.phone));
    }
}
