package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BlogPage {
    StringName stringName = new StringName();
    private final static By NEXT_PAGE = By.xpath("//a[@href='/index.php/news?start=6'']");
    @И("^перейти на вторую страницу$")
    public void fillOutTheOrderForm() {
        $(NEXT_PAGE).scrollTo();
            $(NEXT_PAGE).click();
    }
}
