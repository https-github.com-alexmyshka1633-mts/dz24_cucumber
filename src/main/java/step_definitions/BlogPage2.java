package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class BlogPage2 {
    StringName stringName = new StringName();
    public final By BUTTON_CARD = By.xpath(".//h3[@class='el-title uk-h4 uk-margin-top uk-margin-remove-bottom']");
    public final By ARTICLE_CARD = By.xpath("//h3");
    @И("^перейти на карточку \"(.*)\"$")
    public void selectProduct(String articleName) {
        SelenideElement product = $$(ARTICLE_CARD).findBy(Condition.text(articleName.toString()));
        product.$(BUTTON_CARD).click();
    }
}
